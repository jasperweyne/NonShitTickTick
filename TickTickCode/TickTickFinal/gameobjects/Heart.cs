﻿using Microsoft.Xna.Framework;
using System;
using Microsoft.Xna.Framework.Graphics;

class Heart : SpriteGameObject
{
    int sheetIndex;
    int livesSheet;
    Texture2D heartSprite;

    public Heart(string assetName, int layer, string id, float parentMult) : base("", layer, id, parentMult)
    {
        sheetIndex = 0;
        heartSprite = GameEnvironment.AssetManager.GetSprite(assetName);
    }

    public override void Update(GameTime gameTime)
    {
        Player player = GameWorld.Find("player") as Player;
        livesSheet = 3 - player.Lives;
    }

    public override void Draw(GameTime gameTime, SpriteBatch spriteBatch)
    {
        Rectangle rectangle = new Rectangle(100*livesSheet, 0, 100, 91);
        spriteBatch.Draw(heartSprite, position, rectangle, Color.White,
    0.0f, origin, 1.0f, SpriteEffects.None, 0.0f);
    }
}