using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Input;

partial class Player : AnimatedGameObject
{
    protected Vector2 startPosition;
    protected bool isOnTheGround;
    protected float previousYPosition;
    protected bool isAlive;
    protected bool exploded;
    protected bool finished;
    protected bool walkingOnIce, walkingOnHot;
    protected float laserTime, hurtTime;
    protected int lives;

    public Player(Vector2 start) : base(2, "player")
    {
        LoadAnimation("Sprites/Player/spr_idle", "idle", true); 
        LoadAnimation("Sprites/Player/spr_run@13", "run", true, 0.05f);
        LoadAnimation("Sprites/Player/spr_jump@14", "jump", false, 0.05f); 
        LoadAnimation("Sprites/Player/spr_celebrate@14", "celebrate", false, 0.05f);
        LoadAnimation("Sprites/Player/spr_die@5", "die", false);
        LoadAnimation("Sprites/Player/spr_explode@5x5", "explode", false, 0.04f);

        lives = 3;
        startPosition = start;
        laserTime = 0;
        Reset();
    }

    public override void Reset()
    {
        lives = 3;
        position = startPosition;
        velocity = Vector2.Zero;
        isOnTheGround = true;
        isAlive = true;
        exploded = false;
        finished = false;
        walkingOnIce = false;
        walkingOnHot = false;
        PlayAnimation("idle");
        previousYPosition = BoundingBox.Bottom;
    }

    public override void HandleInput(InputHelper inputHelper)
    {
        float walkingSpeed = 400;
        if (walkingOnIce)
        {
            walkingSpeed *= 1.5f;
        }
        if (!isAlive)
        {
            return;
        }
        if (inputHelper.IsKeyDown(Keys.Left))
        {
            velocity.X = -walkingSpeed;
        }
        else if (inputHelper.IsKeyDown(Keys.Right))
        {
            velocity.X = walkingSpeed;
        }
        else if (!walkingOnIce && isOnTheGround)
        {
            velocity.X = 0.0f;
        }
        if (velocity.X != 0.0f)
        {
            Mirror = velocity.X < 0;
        }
        if ((inputHelper.KeyPressed(Keys.Space) || inputHelper.KeyPressed(Keys.Up)) && isOnTheGround)
        {
            Jump();
        }
        if ((inputHelper.KeyPressed(Keys.S) || inputHelper.KeyPressed(Keys.Down)) && laserTime <= 0)
        {
            laserTime = 0.6f;
            GameWorld.Add(new Laser("Sprites/Player/Green_laser", this));
        }
    }

    public override void Update(GameTime gameTime)
    {
        base.Update(gameTime);
        if(hurtTime > 0)
        {
            hurtTime -= (float)gameTime.ElapsedGameTime.TotalSeconds;
        }
        if(laserTime > 0)
        {
            laserTime -= (float)gameTime.ElapsedGameTime.TotalSeconds;
        }
        if (!finished && isAlive)
        {
            if (isOnTheGround)
            {
                if (velocity.X == 0)
                {
                    PlayAnimation("idle");
                }
                else
                {
                    PlayAnimation("run");
                }
            }
            else if (velocity.Y < 0)
            {
                PlayAnimation("jump");
            }

            TimerGameObject timer = GameWorld.Find("timer") as TimerGameObject;
            if (walkingOnHot)
            {
                timer.Multiplier = 2;
            }
            else if (walkingOnIce)
            {
                timer.Multiplier = 0.5;
            }
            else
            {
                timer.Multiplier = 1;
            }

            TileField tiles = GameWorld.Find("tiles") as TileField;
            if (BoundingBox.Top >= tiles.Rows * tiles.CellHeight)
            {
                lives = 0;
                Die(true);
            }
        }

        DoPhysics();
    }

    public void Explode()
    {
        if (!isAlive || finished)
        {
            return;
        }
        isAlive = false;
        exploded = true;
        velocity = Vector2.Zero;
        position.Y += 15;
        PlayAnimation("explode");
    }

    public void Die(bool falling)
    {
        if (lives > 0 && hurtTime <= 0)
        {
            lives--;
            hurtTime = 1.5f;
            Jump(400);
            return;
        }
        if (!isAlive || finished)
        {
            return;
        }
        if (lives == 0)
        {
            isAlive = false;
            velocity.X = 0.0f;
            if (falling)
            {
                GameEnvironment.AssetManager.PlaySound("Sounds/snd_player_fall");
            }
            else
            {
                velocity.Y = -900;
                GameEnvironment.AssetManager.PlaySound("Sounds/snd_player_die");
            }
            PlayAnimation("die");
        }
    }

    public bool IsAlive
    {
        get { return isAlive; }
    }

    public int Lives
    {
        get { return lives; }
    }

    public bool Finished
    {
        get { return finished; }
    }

    public void LevelFinished()
    {
        finished = true;
        velocity.X = 0.0f;
        PlayAnimation("celebrate");
        GameEnvironment.AssetManager.PlaySound("Sounds/snd_player_won");
    }
}
