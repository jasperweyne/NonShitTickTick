﻿using Microsoft.Xna.Framework;
using System;

class Laser : SpriteGameObject
{
    public Laser(string assetName, Player player) : base(assetName)
    {
        this.Position = player.Position;
        position.Y -= 50;
        if (player.Mirror) velocity.X = -1500;
        else velocity.X = 1500;
        visible = true;
    }

    public override void Reset()
    {
        visible = false;
        velocity.X = 0;
    }

    public override void Update(GameTime gameTime)
    {
        base.Update(gameTime);
        GameObjectList enemies = GameWorld.Find("enemies") as GameObjectList;
        foreach (AnimatedGameObject obj in enemies.Children)
        {
            if (this.CollidesWith(obj))
            {
                obj.Reset();
                this.Reset();
            }
        }
        // check if we are outside the screen
        Rectangle screenBox = new Rectangle(0, 0, GameEnvironment.Screen.X, GameEnvironment.Screen.Y);
        if (!screenBox.Intersects(this.BoundingBox))
        {
            Reset();
        }
    }
}
